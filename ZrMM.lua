-- ZrMM.lua
-- ZrMiniMap by Zerorez

-- Updated by Endareth with Minimap Scaling (via Settings window) 2014-04-06
-- Added toggle to switch between Camera and Character Facing Orientation (Idea by AncientLich) 2014-04-07

ZrMM = {}
ZrMM.def = {}

local MM_pScale
local MM_mScale = 0.5
-----------------------------------------------------------------
-- Updates
-----------------------------------------------------------------
-- delay buffer for updates (thanks Wykkyd :D)
local bt = {}
function MM_DelayBuffer(key, buffer)
	if key == nil then return end
	if bt[key] == nil then bt[key] = {} end
	bt[key].buffer = buffer or 3
	bt[key].now = GetFrameTimeMilliseconds()
	if bt[key].last == nil then bt[key].last = bt[key].now end
	bt[key].diff = bt[key].now - bt[key].last
	bt[key].eval = bt[key].diff >= bt[key].buffer
	if bt[key].eval then bt[key].last = bt[key].now end
	return bt[key].eval
end

function MM_OnUpdate()
	MM_HideCheck()
	if Zr_MM_Scroll:IsHidden() == false then
		MM_MapUpdate()
		MM_ZoneUpdate()
		MM_PinUpdate()
	end
end

function MM_WorldMapUpdate()
	SetMapToPlayerLocation()
	ZO_WorldMap_UpdateMap()
end

function MM_HideCheck()
	if not MM_DelayBuffer("HideDelay", 2) then return; end
	local menuHidden = ZO_KeybindStripControl:IsHidden()
	local interactHidden = ZO_InteractWindow:IsHidden()
	local gameMenuHidden = ZO_GameMenu_InGame:IsHidden()
	if menuHidden == true and interactHidden == true and gameMenuHidden == true then
		MM_Show()
	elseif menuHidden == false or interactHidden == false or gameMenuHidden == false then
		if gameMenuHidden == false then
		MM_Show()
		else
		MM_Hide()
		end
	end
end

function MM_Hide()
	Zr_MM_Scroll:SetHidden(true)
	Zr_MM_Bg:SetHidden(true)
	Zr_MM_Zone:SetHidden(true)
	Zr_MM_Player:SetHidden(true)
	Zr_MM:SetMouseEnabled(false)
end

function MM_Show()
	Zr_MM_Scroll:SetHidden(false)
	Zr_MM_Bg:SetHidden(false)
	Zr_MM_Zone:SetHidden(false)
	Zr_MM_Player:SetHidden(false)
	Zr_MM:SetMouseEnabled(true)
end

function MM_MapUpdate()
	if not MM_DelayBuffer("MapUpdate", 2) then return; end
	MM_MapInfo()
	MM_BuildMap()
	MM_PositionUpdate()
end

function MM_ZoneUpdate()
	if not MM_DelayBuffer("ZoneUpdate", 2) then return; end
	if Zr_MM_Zone:GetText() ~= GetMapName() then
		Zr_MM_Zone:SetText(GetMapName())
	end
end

function MM_PinUpdate()
	if not MM_DelayBuffer("PinUpdate", 100) then return; end
	MM_PinCopy()
end

-- hides all current pins (fires when Zone Text changes)
function MM_ZoneChange()	
	local numChildren = Zr_MM_Scroll_Map_Pins:GetNumChildren()
	for pctrlnum=1, numChildren do
		pctrl = Zr_MM_Scroll_Map_Pins:GetChild(pctrlnum)
		if pctrl.Zone ~= GetMapName() then
			pctrl:ClearAnchors()
			pctrl:SetHidden(true)
		end
	end
	ZO_WorldMap_UpdateMap()
end

function MM_PositionUpdate()
	local x, y, heading = GetMapPlayerPosition("player")
	local mapWidth = Zr_MM_Scroll_Map_0:GetWidth()
	local mmWidth = Zr_MM_Scroll:GetWidth()
	local widthCenter = mmWidth / 2
	local mapWidth = mapWidth * ZrMM.currentMap.Dx
	local mapHeight = Zr_MM_Scroll_Map_0:GetHeight()
	local mmHeight = Zr_MM_Scroll:GetHeight()
	local heightCenter = mmHeight / 2
	local mapHeight = mapHeight * ZrMM.currentMap.Dx 
	local hscroll = x * mapWidth
	local hpos = hscroll - widthCenter
	local vscroll = y * mapHeight
	local vpos = vscroll - heightCenter
	Zr_MM_Scroll:SetHorizontalScroll(hpos)
	Zr_MM_Scroll:SetVerticalScroll(vpos)
	if MM_GetUseCameraOrientation() then
		Zr_MM_Player:SetTextureRotation(GetPlayerCameraHeading())
	else
		Zr_MM_Player:SetTextureRotation(heading)
	end
end

-------------------------------------------------------------
-- Map Building
-------------------------------------------------------------
-- get map info for minimap
function MM_MapInfo()
	ZrMM.currentMap = {} 
	SetMapToPlayerLocation()
	ZrMM.currentMap.name = GetMapName()
	ZrMM.currentMap.tileTexture = GetMapTileTexture()
	ZrMM.currentMap.Dx, ZrMM.currentMap.Dy = GetMapNumTiles()
	ZrMM.currentMap.numTiles = ZrMM.currentMap.Dx * ZrMM.currentMap.Dy
	tileTexture = ZrMM.currentMap.tileTexture
	local stringEnd = string.sub(tileTexture,-6)
	local number = ""
	local nameNoNum = ""
	local path = ""
	
	-- identify the number of the texture (usually 0 or none)
	if string.gmatch(stringEnd, "%d+") ~= nil then
		for mapNum in string.gmatch(stringEnd, "%d+") do
			number = mapNum
		end
	end
	if number == nil then	number = "" else rNumber = tonumber(number) end
	
	-- this pulls the file name from the tileTexture string and the folder path
	local counter = 1
	for word in string.gmatch(tileTexture, "[%w_%-]+") do 
		if counter == 1 then path = word end
		if counter == 2 then path = tostring(path .. "//" .. word) end
		if counter == 3 then path = tostring(path .. "//" .. word) end
		if counter == 4 then name = word end 
		counter = counter + 1
	end
	ZrMM.currentMap.path = path
	
	-- cut number from file name based on digits in number
	if number == "" then
		nameNoNum = tostring(name)
	elseif rNumber < 10 then
		local onedigit = string.sub(name, 1,-2)
		nameNoNum = onedigit
	elseif rNumber > 9 then
		local twodigit = string.sub(name, 1,-3)
		nameNoNum = twodigit
	end
	ZrMM.currentMap.filename = name
	ZrMM.currentMap.nameNoNum = nameNoNum
	
	-- store tile textures in table
	if ZrMM.currentMap.numTiles > 1 then
		ZrMM.currentMap.tiles = {}
		for i=0, ZrMM.currentMap.numTiles do
			local value = tostring(ZrMM.currentMap.path .. "//" .. ZrMM.currentMap.nameNoNum .. i .. ".dds")
			table.insert(ZrMM.currentMap.tiles, value)
		end
	elseif ZrMM.currentMap.numTiles == 1 then
		local value = tostring(ZrMM.currentMap.path .. "//" .. ZrMM.currentMap.name .. ".dds")
		table.insert(ZrMM.currentMap.tiles, value)
	end
end

-- gets tile scale for map
function MM_TileScale()
	local texW, texH = Zr_MM_Scroll_Map_0:GetTextureFileDimensions()
	local dx, dy = GetMapNumTiles()
	local fullMapWidth = texW * dx
	local mapsize = MM_mScale * fullMapWidth
	local tileX = mapsize / dx
	local tileY = mapsize / dy
	return tileX,tileY
end

-- build the minimap
function MM_BuildMap()
	local xTiles = ZrMM.currentMap.Dx + 1
	-- create tile controls if they don't exist
	for i,v in pairs(ZrMM.currentMap.tiles) do
		local ctrlNum = i - 1
		local tileControl = tostring("Zr_MM_Scroll_Map_" .. ctrlNum)
		if GetControl(tileControl) == nil then
			tCtrl = WINDOW_MANAGER:CreateControl(tileControl, Zr_MM_Scroll_Map, CT_TEXTURE)
			tCtrl:SetDimensions(MM_TileScale())
		end
	end
	
	-- arrange and anchor tiles
	for i=1, 50 do
		if i == 1 then
			Zr_MM_Scroll_Map_0:SetTexture(ZrMM.currentMap.tiles[i])
			MM_TileSize = MM_TileScale()
			Zr_MM_Scroll_Map_0:SetDimensions(MM_TileScale())
		elseif i < xTiles then
			ctrlNum = i - 1
			prevCtrlNum = i - 2
			tileNum = tostring("_" .. ctrlNum)
			tileControl = Zr_MM_Scroll_Map:GetNamedChild(tileNum)
			prevCtrl = tostring("_" .. prevCtrlNum)
			prevTileCtrl = Zr_MM_Scroll_Map:GetNamedChild(prevCtrl)
			tileControl:SetHidden(false)
			tileControl:SetTexture(ZrMM.currentMap.tiles[i])
			tileControl:SetAnchor(TOPLEFT,prevTileCtrl,TOPRIGHT,0,0)
			tileControl:SetDimensions(MM_TileScale())
		elseif i > ZrMM.currentMap.Dx then
			ctrlNum = i - 1
			anchorCtrlNum = ctrlNum - ZrMM.currentMap.Dx
			tileNum = tostring("_" .. ctrlNum)
			tileControl = Zr_MM_Scroll_Map:GetNamedChild(tileNum)
			if tileControl ~= nil then
			anchorNum = tostring("_" .. anchorCtrlNum)
			anchorControl = Zr_MM_Scroll_Map:GetNamedChild(anchorNum)
			tileControl:SetHidden(false)
			tileControl:SetTexture(ZrMM.currentMap.tiles[i])
			tileControl:SetAnchor(TOP,anchorControl,BOTTOM,0,0)
			tileControl:SetDimensions(MM_TileScale())
			end
		end
		
		if i > ZrMM.currentMap.numTiles then
			tileNum = i - 1
			tileName = tostring("_" .. tileNum)
			if Zr_MM_Scroll_Map:GetNamedChild(tileName) ~= nil then
				tileControl = Zr_MM_Scroll_Map:GetNamedChild(tileName)
				tileControl:SetHidden(true)
			end
		end
	end
	-- set map dimensions
	local mWidth = MM_TileSize * ZrMM.currentMap.Dx
	local mHeight = MM_TileSize * ZrMM.currentMap.Dx
	Zr_MM_Scroll_Map:SetDimensions(mWidth, mHeight)
end


-----------------------------------------------------------------
-- Map Pins
-----------------------------------------------------------------
function MM_PinCopy()
	local numChildren = ZO_WorldMapContainer:GetNumChildren()
	ZrMM.zoPins = {}
	for child=1, numChildren do
		childControl = ZO_WorldMapContainer:GetChild(child)
		if childControl.m_Pin ~= nil and childControl.m_Pin:GetUnitTag() ~= 'player' then
			table.insert(ZrMM.zoPins, childControl)
		end
	end
	MM_PinHidden()
	
	ZrMM.pins = {}
	for i,v in pairs(ZrMM.zoPins) do
		if v.m_Pin.normalizedX ~= nil then
		local pinName = v:GetName() -- get ZO_MapPin Name
		ZrMM.pins[pinName] = {} -- create table to store pin data
		local zrpin = ZrMM.pins[pinName] -- shortcut alias for table
		zrpin.ctrlName = tostring("Zr_MM_Scroll_Map_Pins" .. pinName)
		
		-- if a control with the name exists get it otherwise create one
		if Zr_MM_Scroll_Map_Pins:GetNamedChild(pinName) == nil then
			pin = WINDOW_MANAGER:CreateControl(zrpin.ctrlName, Zr_MM_Scroll_Map_Pins, CT_TEXTURE)
		else 
			pin = Zr_MM_Scroll_Map_Pins:GetNamedChild(pinName)
		end
		
		-- calculate pin coords from normalized coords
		mWidth, mHeight = Zr_MM_Scroll_Map:GetDimensions()
		zrpin.nX = v.m_Pin.normalizedX
		zrpin.nY = v.m_Pin.normalizedY
		zrpin.X = zrpin.nX * mWidth 
		zrpin.Y = zrpin.nY * mHeight
		
		zrpin.control = pin
		zrpin.zocontrol = v
		
		local mPin = v.m_Pin
		
		-- get pin texture (thanks to Shinni)
		if mPin.m_PinType ~= nil then
			if mPin.pinBlobKey ~= nil then
				zrpin.texture = mPin.pinBlobTexture
			elseif type(mPin.PIN_DATA[mPin.m_PinType].texture) == 'string' then
				zrpin.texture = mPin.PIN_DATA[mPin.m_PinType].texture
			elseif type(mPin.PIN_DATA[mPin.m_PinType].texture) == 'function' then
				zrpin.texture = mPin.PIN_DATA[mPin.m_PinType].texture(mPin)
			end
			
		end
		
		pin:SetHidden(v.Zone)
		pin:SetTexture(zrpin.texture)
		pin:SetDimensions(MM_pScale, MM_pScale)
		pin:SetAnchor(CENTER, Zr_MM_Scroll_Map_Pins, TOPLEFT, zrpin.X, zrpin.Y)
		
		if mPin:IsQuest() == ture then
			pin:SetDrawLayer(1)
		else
			pin:SetDrawLayer(2)
		end
		
		pin:SetMouseEnabled(true)
		pin.Zone = GetMapName()
		
		end
	end
end

function MM_PinMouseEnter()
	
end


-- checks to see if a pins are hidden on the world map
function MM_PinHidden()
	if ZO_WorldMap:IsHidden() == true then
		ZO_WorldMap:SetAlpha(0)
		ZO_WorldMap:SetHidden(false)
		for i,v in ipairs(ZrMM.zoPins) do
			v.Zone = v:IsHidden()
		end
		ZO_WorldMap:SetAlpha(1)
		ZO_WorldMap:SetHidden(true)
	else
		for i,v in ipairs(ZrMM.zoPins) do
			v.Zone = v:IsHidden()
		end
	end
end

------------------------------------------------------------
-- Cyrodiil
------------------------------------------------------------

function MM_IsCurrentMapCyrodiil()
	
end


------------------------------------------------------------
-- Settings
-------------------------------------------------------------
function MM_GetMapSize()
	local mwidth = Zr_MM_Scroll:GetWidth()
	return mwidth
end

function MM_SetMapSize(newSize)
	Zr_MM_Scroll:SetDimensions(newSize,newSize)
	Zr_MM:SetDimensions(newSize,newSize)
	ZrMM.SV.MapSize = newSize
end

function MM_GetMapAlpha()
	return ZrMM.SV.MapAlpha
end

function MM_SetMapAlpha(newAlpha)
	local newAlphaPercent = newAlpha / 100
	Zr_MM:SetAlpha(newAlphaPercent)
	ZrMM.SV.MapAlpha = newAlpha
end

function MM_GetPinScale()
	local scale = ZrMM.SV.PinScale
	return scale
end

function MM_SetPinScale(newScale)
	MM_pScale = newScale
	for i,v in ipairs(ZrMM.pins) do
		local MMpin = v.control
		MMpin:SetDimensions(newScale, newScale)
	end
	ZrMM.SV.PinScale = newScale
end

function MM_GetMapScale()
	local scale = ZrMM.SV.MapScale
	return scale
end

function MM_SetMapScale(newScale)
	MM_mScale = newScale / 100.0
	ZrMM.SV.MapScale = newScale
end

function MM_GetHideCompass()
	return ZrMM.SV.hideCompass
end

function MM_SetHideCompass(checkbox)
	ZO_Compass:SetHidden(checkbox)
	ZrMM.SV.hideCompass = checkbox
end

function MM_GetUseCameraOrientation()
	return ZrMM.SV.UseCameraOrientation
end

function MM_SetUseCameraOrientation(checkbox)
	ZrMM.SV.UseCameraOrientation = checkbox
end



-------------------------------------------------------------
-- On Initialized
-------------------------------------------------------------

function MM_LoadSavedVars()	
	if ZrMM.SV.position ~= nil then
		local pos = {}
		pos.anchorTo = GetControl(pos.anchorTo)
		Zr_MM:SetAnchor(ZrMM.SV.position.point, pos.anchorTo, ZrMM.SV.position.relativePoint, ZrMM.SV.position.offsetX, ZrMM.SV.position.offsetY)
		Zr_MM_Bg:SetAnchorFill(Zr_MM)
	end
	if ZrMM.SV.MapSize ~= nil then MM_SetMapSize(ZrMM.SV.MapSize) end
	if ZrMM.SV.MapAlpha ~= nil then MM_SetMapAlpha(ZrMM.SV.MapAlpha) end
	if ZrMM.SV.PinScale ~= nil then MM_SetPinScale(ZrMM.SV.PinScale) end
	if ZrMM.SV.MapScale ~= nil then MM_SetMapScale(ZrMM.SV.MapScale) end
	if ZrMM.SV.hideCompass ~= nil then MM_SetHideCompass(ZrMM.SV.hideCompass) end
end

function MM_OnInit(eventCode, addOnName)
	if addOnName ~= "ZrMM" then return end
	
	-- Load saved variables (thanks to ZeakFury)
	ZrMM.Defaults = {
		["MapSize"] = 200,
		["position"] = {
			["point"] = TOPLEFT,
			["relativePoint"] = TOPLEFT,
			["offsetX"] = 0,
			["offsetY"] = 0
		},
		["MapAlpha"] = 100,
		["PinScale"] = 40,
		["MapScale"] = 60,
		["UseCameraOrientation"] = true
	}
	ZrMM.SV = ZO_SavedVars:New( "ZrMM" , 5 , nil , ZrMM.Defaults , nil )
	if ZrMM.SV ~= nil then
		MM_LoadSavedVars()
	end
	
	Zr_MM:SetResizeHandleSize(MOUSE_CURSOR_RESIZE_NS)
	Zr_MM:SetHandler("OnMouseUp", function(self) 
		local size = Zr_MM:GetWidth()
		MM_SetMapSize(size)
		ZrMM.SV.position.offsetX = Zr_MM:GetLeft()
		ZrMM.SV.position.offsetY = Zr_MM:GetTop()
		end)
	Zr_MM_Scroll:SetScrollBounding(0)
	Zr_MM_Player:SetTexture("EsoUI//Art/MapPins//UI-WorldMapPlayerPip.dds")
	Zr_MM_Zone:SetHandler("OnTextChanged", MM_ZoneChange)
	
	ZO_WorldMapInfo:SetHandler("OnShow", MM_WorldMapUpdate)
	Zr_MM_Player:SetHandler("OnShow", MM_WorldMapUpdate)
	
	-- options panel
	LAM = LibStub:GetLibrary("LibAddonMenu-1.0")
	ZrMMpanel = LAM:CreateControlPanel("MM_OptionsPanel", "ZrMiniMap")
	LAM:AddHeader(ZrMMpanel, "MM_OptionsHeader", "Options")
	LAM:AddButton(ZrMMpanel, "MM_Option_ReloadPins", "Reload Pins", "Forces reload of MiniMap pins", MM_ZoneChange)
        LAM:AddSlider(ZrMMpanel, "MM_Option_Size", "MiniMap Size", "Change the size of the MiniMap", 100, 1000, 100, MM_GetMapSize, MM_SetMapSize)
	LAM:AddSlider(ZrMMpanel, "MM_Option_Alpha", "MiniMap Alpha", "Change the transparency of the MiniMap", 0, 100, 1, MM_GetMapAlpha, MM_SetMapAlpha)
	LAM:AddSlider(ZrMMpanel, "MM_Option_PinScale", "Pin Scale", "Adjust the scale of MiniMap pins", 0, 100, 1, MM_GetPinScale, MM_SetPinScale)
	LAM:AddSlider(ZrMMpanel, "MM_Option_MapScale", "Map Scale", "Adjust the scale of the MiniMap", 0, 200, 1, MM_GetMapScale, MM_SetMapScale)
	LAM:AddCheckbox(ZrMMpanel, "MM_Option_HideCompass", "Hide Compass", "Hide the default UI's compass", MM_GetHideCompass, MM_SetHideCompass)
	LAM:AddCheckbox(ZrMMpanel, "MM_Option_UseCameraOrientation", "Use Camera Orientation", "Pointer follows camera instead of character facing", MM_GetUseCameraOrientation, MM_SetUseCameraOrientation)
end

EVENT_MANAGER:RegisterForEvent( "ZrMiniMap", EVENT_ADD_ON_LOADED , MM_OnInit)
